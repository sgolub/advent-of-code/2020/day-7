package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const dataKey = "bag_rules"

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 7
}

type baggage interface {
	ParseContainment(val string) error
	Color() string
	Pattern() string
	Contains() int
	Name() string
	CanContain(color, pattern string) bool
}

type bag struct {
	color   string
	pattern string
	Contain map[baggage]int
}

var (
	bagRegistry = map[string]map[string]*bag{}
	bagCount    *regexp.Regexp
)

func getBagFromRegistry(color, pattern string) *bag {
	colorGroup, ok := bagRegistry[color]
	if !ok {
		colorGroup = map[string]*bag{}
		bagRegistry[color] = colorGroup
	}
	patternedBag, ok := colorGroup[pattern]
	if !ok {
		patternedBag = &bag{
			color:   color,
			pattern: pattern,
		}
		colorGroup[pattern] = patternedBag
	}
	if patternedBag.Contain == nil {
		patternedBag.Contain = map[baggage]int{}
	}
	return patternedBag
}

func (b bag) Name() string {
	return fmt.Sprintf("%s %s", b.pattern, b.color)
}

func (b bag) String() string {
	containText := "no other bags"
	if b.Contain != nil {
		cParts := []string{}
		for bg, c := range b.Contain {
			btext := "bag"
			if c > 1 {
				btext += "s"
			}
			cParts = append(cParts, fmt.Sprintf("%d %s %s", c, bg.Name(), btext))
		}
		if len(cParts) > 0 {
			containText = strings.Join(cParts, ", ")
		}
	}
	return fmt.Sprintf("%s bags contain %s.", b.Name(), containText)
}

func (b bag) Color() string {
	return b.color
}

func (b bag) Pattern() string {
	return b.pattern
}

func (b bag) Contains() int {
	count := 0
	if b.Contain == nil {
		return 0
	}
	for bg, i := range b.Contain {
		c := bg.Contains()
		count += i*c + i
	}
	return count
}

func (b bag) CanContain(color, pattern string) bool {
	if b.Contain == nil {
		return false
	}
	for subBag := range b.Contain {
		if subBag.Color() == color && subBag.Pattern() == pattern {
			return true
		}
		if subBag.CanContain(color, pattern) {
			return true
		}
	}
	return false
}

func (b bag) ParseContainment(val string) error {
	if val != "" && val != "no other bags" {
		if b.Contain == nil {
			b.Contain = map[baggage]int{}
		}
		if bagCount == nil {
			var err error
			bagCount, err = regexp.Compile("(\\d) (\\w+) (\\w+) bags?")
			if err != nil {
				return fmt.Errorf("failed to compile bag regex: %w", err)
			}
		}
		specs := strings.Split(strings.TrimSuffix(val, "."), ",")
		for _, spec := range specs {
			if bagCount.MatchString(spec) {
				matches := bagCount.FindAllStringSubmatch(strings.TrimSpace(spec), -1)
				if len(matches) != 1 || len(matches[0]) != 4 {
					continue
				}
				m := matches[0]
				cB := getBagFromRegistry(m[3], m[2])
				i, err := strconv.Atoi(m[1])
				if err != nil {
					continue
				}
				b.Contain[cB] = i
			}
		}
	}
	return nil
}

func newBagFromText(val string) (*bag, error) {
	splits := strings.Split(val, " contain ")
	if len(splits) != 2 {
		return nil, fmt.Errorf("invalid bag text: %s", val)
	}
	bagSplits := strings.Split(splits[0], " ")
	if len(bagSplits) != 3 || bagSplits[2] != "bags" {
		return nil, fmt.Errorf("invalid bag description: %s", splits[0])
	}
	b := getBagFromRegistry(bagSplits[1], bagSplits[0])
	b.ParseContainment(splits[1])
	return b, nil
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) []string {
	result := make([]string, 0)
	data, ok := days.Control().LoadData(d, useSampleData)[dataKey]
	if !ok {
		return result
	}
	for _, i := range data.([]interface{}) {
		result = append(result, i.(string))
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D7P1")
	for _, rule := range data {
		newBagFromText(rule)
	}
	count := 0
	for _, patternSub := range bagRegistry {
		for _, b := range patternSub {
			if b.CanContain("gold", "shiny") {
				count++
			}
		}
	}
	return fmt.Sprint(count)
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D7P2")
	for _, rule := range data {
		newBagFromText(rule)
	}
	return fmt.Sprint(getBagFromRegistry("gold", "shiny").Contains())
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
